import { IsNotEmpty, Length, IsPositive } from 'class-validator';
export class CreateStoreDto {
  @IsNotEmpty()
  @Length(2, 1000)
  name: string;

  @IsPositive()
  @IsNotEmpty()
  address: number;

  @IsNotEmpty()
  @Length(9, 10)
  tel: string;

  @IsNotEmpty()
  @Length(0, 1)
  gender: string;
}
